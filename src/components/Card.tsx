import React from "react";
import ICountry from "../interface/ICountry";

const Card = (props: { country: ICountry }) => {
  const country = props.country;

  return (
    <li className="card">
      <img src={country.flags.svg} alt="alt-drapeau" />
      <div className="infos">
        <h2>{country.translations.fra.common}</h2>
        <h4>{country.capital}</h4>
        <p>Pop. {country.population.toLocaleString()}</p>
      </div>
    </li>
  );
};

export default Card;
