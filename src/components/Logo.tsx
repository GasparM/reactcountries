import React from "react";

const Logo = () => {
  return (
    <div className="logo">
      <img src="./logo192.png" alt="alt-logo"></img>
      <h3>Damn le react</h3>
    </div>
  );
};

export default Logo;
