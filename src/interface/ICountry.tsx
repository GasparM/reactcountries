interface ICountry {
  continents: Array<string>;
  population: number;
  translations: ITranslation;
  capital: Array<string>;
  flags: IFlags;
}

interface ITranslation {
  fra: {
    common: string;
  };
}

interface IFlags {
  png: string;
  svg: string;
}

export default ICountry;
