import React, { useContext } from "react";
import Logo from "../components/Logo";
import Navigation from "../components/Navigation";
import UserContext from "../components/UserContext";

const About = () => {
  const [user] = useContext(UserContext);

  return (
    <div className="Header">
      <pre>{JSON.stringify(user, null, 2)}</pre>
      <Logo />
      <Navigation />
      <h1>Damn c'est about</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita
        inventore placeat voluptatem cumque facere amet, quis, neque pariatur a
        natus error, veniam quos rem ipsum quibusdam excepturi deserunt cum
        quaerat!
      </p>
    </div>
  );
};

export default About;
