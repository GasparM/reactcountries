import React from "react";
import Navigation from "../components/Navigation";
import Logo from "../components/Logo";

const Error = () => {
  return (
    <div className="content">
      <Logo />
      <Navigation />
      <h1>Error 404</h1>
    </div>
  );
};

export default Error;
