import React, { useContext } from "react";
import Countries from "../components/Countries";
import Logo from "../components/Logo";
import Navigation from "../components/Navigation";
import UserContext from "../components/UserContext";
import { login } from "../data/login";
import { Button } from "antd";

const Home = () => {
  const [user, setUser] = useContext(UserContext);

  return (
    <div className="Header">
      <div className="login">
        <pre>{JSON.stringify(user, null, 2)}</pre>
        {user ? (
          <Button type="primary" onClick={() => setUser(null)}>
            Déconnexion
          </Button>
        ) : (
          <Button
            type="primary"
            onClick={async () => {
              const user = await login();
              setUser(user);
            }}
          >
            login
          </Button>
        )}
      </div>

      <Logo />
      <Navigation />
      <Countries />
    </div>
  );
};

export default Home;
